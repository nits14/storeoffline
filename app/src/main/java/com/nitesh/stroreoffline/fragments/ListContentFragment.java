package com.nitesh.stroreoffline.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nitesh.stroreoffline.AddEntryActivity;
import com.nitesh.stroreoffline.MainActivity;
import com.nitesh.stroreoffline.R;
import com.nitesh.stroreoffline.adapter.ListContentAdapter;
import com.nitesh.stroreoffline.holder.Constants;
import com.nitesh.stroreoffline.holder.dataModal;

import java.util.ArrayList;

/**
 * Created by Admin on 007 07-10-2017.
 */

public class ListContentFragment extends Fragment implements ListContentAdapter.OnItemClickListener {
    private static int position;
    private final String TAG = this.getClass().getName();
    //private ArrayList<dataModal> ModalList;
    private ArrayList<dataModal> ModalList2;
    private static String currentTitle;
    private ListContentAdapter adapter;

    public ListContentFragment() {}

    public static ListContentFragment newInstance(ArrayList<dataModal> dtmodal,int i, String message){
        ListContentFragment f = new ListContentFragment();
        currentTitle = message;
        position = i;
        Bundle bdl = new Bundle(1);
        bdl.putInt(Constants.KEY_FRAG_INDEX,i);
        bdl.putParcelableArrayList(Constants.KEY_FRAG,dtmodal);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ModalList2 = getArguments().getParcelableArrayList(Constants.KEY_FRAG);
        adapter = new ListContentAdapter(this,getActivity(), ModalList2, currentTitle);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {/*can do stuff*/}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.frag_list_content, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(),1);
        recyclerView.setLayoutManager(mLayoutManager);
       // recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
       // recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        rootView.setTag(TAG);
        return rootView;
    }

    @Override
    public void onItemClicked(dataModal dt) {
        Intent intent = new Intent(getActivity(), AddEntryActivity.class);
        intent.putExtra(Constants.Key_open_id,dt);
        getActivity().startActivityForResult(intent,Constants.REQUEST_CODE_RELOAD);
        //startActivity(intent);
    }
}
