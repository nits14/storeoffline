package com.nitesh.stroreoffline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nitesh.stroreoffline.database.DatabaseHandler;
import com.nitesh.stroreoffline.holder.Constants;
import com.nitesh.stroreoffline.holder.dataModal;

import java.util.ArrayList;
import java.util.List;

public class AddEntryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener   {

    private final String TAG = this.getClass().getName();
    private EditText etTitle,etWebpage,etUserId,etPassword,etExtra;
    private boolean isAllOK = false;
    private String category ="";
    private dataModal dtModal;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entry);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        spinner = (Spinner) findViewById(R.id.spinner);
        etTitle = (EditText) findViewById(R.id.ettitle);
        etWebpage = (EditText) findViewById(R.id.etaddress);
        etUserId = (EditText) findViewById(R.id.etusername);
        etPassword = (EditText) findViewById(R.id.etpassword);
        etExtra = (EditText)findViewById(R.id.etextra);
// Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Social");
        categories.add("Ecommerce");
        categories.add("Banking");
        categories.add("Other");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        try {
            Intent i = getIntent();
            dtModal = i.getParcelableExtra(Constants.Key_open_id);
            if (dtModal != null){
                etTitle.setText(dtModal.get_title());
                etWebpage.setText(dtModal.get_webpage());
                etUserId.setText(dtModal.get_userid());
                etPassword.setText(dtModal.get_password());
                etExtra.setText(dtModal.get_extra());
                spinner.setSelection(categories.indexOf(dtModal.get_category()));
                if (spinner.getSelectedItemPosition()==3){
                    etExtra.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_save, menu);
        return true;
    }
    private void insertDB(dataModal dataModal){
        DatabaseHandler.getInstance(getApplicationContext()).addToSQL(dataModal);
    }
    private void updateDB(dataModal dtm){
        DatabaseHandler.getInstance(getApplicationContext()).updateRow(dtm);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.click_save:
                if (checkAllAreValid()){
                    Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
                    /*TODO error*/
                    if (dtModal==null) {    //if records are there update otherwise insert
                        dataModal dt = new dataModal(category, etTitle.getText().toString(), etWebpage.getText().toString(), etUserId.getText().toString(), etPassword.getText().toString(), etExtra.getText().toString());
                        insertDB(dt);
                    }else {
                        dataModal dt = new dataModal(dtModal.get_id(),category,etTitle.getText().toString(),etWebpage.getText().toString(),etUserId.getText().toString(),etPassword.getText().toString(),etExtra.getText().toString());
                        updateDB(dt);
                    }
                    //finish();
                    Intent intent = getIntent();
                    intent.putExtra("key", "ok");
                    setResult(Constants.REQUEST_CODE_RELOAD, intent);
                    finish();
                }
                return true;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private Boolean checkAllAreValid() {
        if (etTitle.getText().length()>2){
            if (etUserId.getText().length()>2){
                if (etPassword.getText().length()>2){
                    return true;
                }else {
                    makeToast("Check Password!");
                }
            }else {
                makeToast("Check Username!");
            }
        }else {
            makeToast("Use Title Must!");
        }
        return false;
    }
    void makeToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        category = adapterView.getItemAtPosition(i).toString();
        if (i==3){
            etExtra.setVisibility(View.VISIBLE);
        }else
            etExtra.setVisibility(View.GONE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        category = adapterView.getItemAtPosition(0).toString();
    }
}
