package com.nitesh.stroreoffline.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.nitesh.stroreoffline.fragments.ListContentFragment;
import com.nitesh.stroreoffline.holder.dataModal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 007 07-10-2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mFragmentTitleList = new ArrayList<>();
    private ArrayList<dataModal> dataModals = new ArrayList<>();
    private ArrayList<dataModal> modalList2 ;

    public ViewPagerAdapter(ArrayList<dataModal> modalList,FragmentManager fm,ArrayList<Fragment> fArrayList,ArrayList<String> mTitleList) {
        super(fm);
        this.dataModals = modalList;
        this.mFragmentList = fArrayList;
        this.mFragmentTitleList = mTitleList;
    }


    @Override
    public Fragment getItem(int position) {
        Log.d("position", Integer.toString(position));
        modalList2 =  new ArrayList<>();
        for (dataModal dt:dataModals){
            if (mFragmentTitleList.get(position).equals(dt.get_category())){
                modalList2.add(dt);
            }
        }
        return ListContentFragment.newInstance(modalList2,position,mFragmentTitleList.get(position));

    }
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
