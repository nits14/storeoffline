package com.benchmarksolution.stroreoffline;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.benchmarksolution.stroreoffline.adapter.ViewPagerAdapter;
import com.benchmarksolution.stroreoffline.database.DatabaseHandler;
import com.benchmarksolution.stroreoffline.fragments.ListContentFragment;
import com.benchmarksolution.stroreoffline.holder.Constants;
import com.benchmarksolution.stroreoffline.holder.dataModal;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private static int REQUEST_CODE_RELOAD = 100;
    private ArrayList<dataModal> ModalList;
    private ArrayList<dataModal> ModalList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {

            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setElevation(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setFillViewport(true);
        // Find the root view
        View root = viewPager.getRootView();
        // Set the color
        root.setBackgroundColor(getResources().getColor(R.color.activity));
        setupViewPager(viewPager);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(MainActivity.this,AddEntryActivity.class));
                startActivityForResult(new Intent(MainActivity.this,AddEntryActivity.class),REQUEST_CODE_RELOAD);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       // super.onActivityResult(requestCode, resultCode, data);
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE_RELOAD  && resultCode  == RESULT_OK) {
                String requiredValue = data.getStringExtra("Key");
                viewPager.getAdapter().notifyDataSetChanged();
            }
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setupViewPager(ViewPager viewPager) {
        //String counttab[] = {"Social","Ecommerce","Banking","Other"};
        ArrayList<String> titles = new ArrayList<>();
        titles.add("Social");
        titles.add("Ecommerce");
        titles.add("Banking");
        titles.add("Other");
        ModalList = DatabaseHandler.getInstance(getApplicationContext()).getAllEntrys();
        ArrayList<Fragment> mFragments = new ArrayList<>();
        for(int i=0;i<titles.size();i++) {
            mFragments.add(new ListContentFragment());
        }


        ViewPagerAdapter adapter = new ViewPagerAdapter(ModalList,getSupportFragmentManager(),mFragments,titles);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
