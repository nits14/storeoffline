package com.benchmarksolution.stroreoffline;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.benchmarksolution.stroreoffline.custom.KeyboardView;
import com.benchmarksolution.stroreoffline.database.PreferenceUtils;
import com.benchmarksolution.stroreoffline.holder.Constants;

public class MasterLogin extends AppCompatActivity {

    KeyboardView keyboardView;
    TextView txtLabel;
    Boolean isKeyStore = false;
    Integer tempPass =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_login);
        txtLabel = (TextView) this.findViewById(R.id.txtenter);
        keyboardView = (KeyboardView) this.findViewById(R.id.keyboard_view);
        isKeyStore =PreferenceUtils.getBoolean(Constants.KEY_IS_PASS,false,getApplicationContext());
        if (isKeyStore){
            txtLabel.setText(R.string.enter_passcode);
        }else txtLabel.setText(R.string.set_passcode);

       keyboardView.mPasswordField.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               if (!isKeyStore && tempPass==0){                //checking passcode is store
                   tempPass = s.length()> 3 ? Integer.parseInt(s.toString()): 0; //storing temp passcode
                   if (tempPass !=0){
                   keyboardView.mPasswordField.setText(null); //clear the edittext for re-enter
                   txtLabel.setText(R.string.set_passcode_reenter);
                       return;
                   }
               }else if (!isKeyStore && tempPass!=0){
                   Integer tempPass2 = s.length()>3 ? Integer.parseInt(s.toString()):0;
                   if (tempPass2.equals(tempPass)){
                       PreferenceUtils.putInt(Constants.Key_passcode,tempPass,getApplicationContext());
                       PreferenceUtils.putBoolean(Constants.KEY_IS_PASS,true,getApplicationContext());
                       isKeyStore = true;
                   }
               }

                if (s.length()==4 && isKeyStore){
                   Integer varr = PreferenceUtils.getInt(Constants.Key_passcode,0,getApplicationContext());
                   if (!varr.equals(0)){
                       if (Integer.parseInt(s.toString())==varr)
                           gotoNext(true);
                       else{
                           showError();
                           mHandler.postDelayed(errorMsgRun, 1000);

                       }
                   }
                }
           }

           @Override
           public void afterTextChanged(Editable s) {

           }
       });
    }
    private Handler mHandler = new Handler();
    private Runnable errorMsgRun = new Runnable() {
        public void run() {
            // Do some stuff that you want to do here
            keyboardView.mPasswordField.requestFocus();
            keyboardView.mPasswordField.setError("Try again");
            // You could do this call if you wanted it to be periodic:
            //mHandler.postDelayed(this, 5000 );

        }
    };

    private void showError() {
        {
            Animation shake = AnimationUtils.loadAnimation(MasterLogin.this, R.anim.shake);
            keyboardView.mPasswordField.startAnimation(shake);
        }
    }

    private void gotoNext(Boolean allOK){
     if (allOK){
        startActivity(new Intent(MasterLogin.this,MainActivity.class));
         finish();
     }
 }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        keyboardView.mPasswordField.addTextChangedListener(null);
    }
}
