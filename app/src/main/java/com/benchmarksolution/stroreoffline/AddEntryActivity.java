package com.benchmarksolution.stroreoffline;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.benchmarksolution.stroreoffline.database.DatabaseHandler;
import com.benchmarksolution.stroreoffline.holder.dataModal;

import java.util.ArrayList;
import java.util.List;

public class AddEntryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener   {

    private final String TAG = this.getClass().getName();
    private EditText etTitle,etWebpage,etUserId,etPassword;
    private boolean isAllOK = false;
    private String category ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_entry);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        etTitle = (EditText) findViewById(R.id.ettitle);
        etWebpage = (EditText) findViewById(R.id.etaddress);
        etUserId = (EditText) findViewById(R.id.etusername);
        etPassword = (EditText) findViewById(R.id.etpassword);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Social");
        categories.add("Ecommerce");
        categories.add("Banking");
        categories.add("Other");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_save, menu);
        return true;
    }
    private void insertDB(dataModal dataModal){
        Log.d("Insert: ", "Inserting ..");
        DatabaseHandler.getInstance(getApplicationContext()).addToSQL(dataModal);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.click_save:
                if (checkAllAreValid()){
                    Toast.makeText(this, "ooooOKoooo", Toast.LENGTH_SHORT).show();
                    dataModal dt = new dataModal(category,etTitle.getText().toString(),etWebpage.getText().toString(),etUserId.getText().toString(),etPassword.getText().toString());
                    insertDB(dt);
                    //finish();

                    Intent intent = getIntent();
                    intent.putExtra("key", "ok");
                    setResult(RESULT_OK, intent);
                    finish();
                }
                return true;
            case android.R.id.home:
                finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean checkAllAreValid() {
        if (etTitle.getText().length()>2){
            if (etUserId.getText().length()>2){
                if (etPassword.getText().length()>2){
                    return true;
                }else {
                    makeToast("Check Password!");
                }
            }else {
                makeToast("Check Username!");
            }
        }else {
            makeToast("Use Title Must!");
        }
        return false;
    }
    void makeToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        category = adapterView.getItemAtPosition(i).toString();

        // Showing selected spinner item
        //Toast.makeText(adapterView.getContext(), "Selected: " + category, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        category = adapterView.getItemAtPosition(0).toString();
    }
}
