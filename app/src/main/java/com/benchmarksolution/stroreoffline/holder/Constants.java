package com.benchmarksolution.stroreoffline.holder;

/**
 * Created by Admin on 005 05-10-2017.
 */

public class Constants {

    public static final String KEY_FRAG = "keyfragment";
    public static final String KEY_FRAG_INDEX = "keyfragindex";

    public static final String DATABASE_NAME = "stroredb";
    public static final String DB_table = "stroretable";
    public static final String DB_id = "rid";
    public static final String DB_category = "caregory";
    public static final String DB_title = "title";
    public static final String DB_webpage = "webpage";
    public static final String DB_userid = "userid";
    public static final String DB_password = "password";

    public static final int DATABASE_VERSION = 1;

    public static final String KEY_IS_PASS = "keyispass";
    public static final String Key_passcode = "passcode";
}
