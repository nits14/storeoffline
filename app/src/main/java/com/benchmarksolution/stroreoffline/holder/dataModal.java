package com.benchmarksolution.stroreoffline.holder;

import android.os.Parcel;
import android.os.Parcelable;

public class dataModal implements Parcelable {
    private int _id;
    private String _category;
    private String _title;
    private String _webpage;
    private String _userid;
    private String _password;

    public dataModal() {
    }

    public dataModal(int _id, String _category, String _title, String _webpage, String _userid, String _password) {
        this._id = _id;
        this._category = _category;
        this._title = _title;
        this._webpage = _webpage;
        this._userid = _userid;
        this._password = _password;
    }

    public dataModal(String _category, String _title, String _webpage, String _userid, String _password) {
        this._category = _category;
        this._title = _title;
        this._webpage = _webpage;
        this._userid = _userid;
        this._password = _password;
    }

    protected dataModal(Parcel in) {
        _id = in.readInt();
        _category = in.readString();
        _title = in.readString();
        _webpage = in.readString();
        _userid = in.readString();
        _password = in.readString();
    }

    public static final Creator<dataModal> CREATOR = new Creator<dataModal>() {
        @Override
        public dataModal createFromParcel(Parcel in) {
            return new dataModal(in);
        }

        @Override
        public dataModal[] newArray(int size) {
            return new dataModal[size];
        }
    };

    public String get_category() {
        return _category;
    }

    public void set_category(String _category) {
        this._category = _category;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_webpage() {
        return _webpage;
    }

    public void set_webpage(String _webpage) {
        this._webpage = _webpage;
    }

    public String get_userid() {
        return _userid;
    }

    public void set_userid(String _userid) {
        this._userid = _userid;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(_category);
        dest.writeString(_title);
        dest.writeString(_webpage);
        dest.writeString(_userid);
        dest.writeString(_password);
    }
}
