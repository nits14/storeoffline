package com.benchmarksolution.stroreoffline.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.benchmarksolution.stroreoffline.R;
import com.benchmarksolution.stroreoffline.holder.dataModal;

import java.util.ArrayList;


/**
 * Created by Admin on 009 09-10-2017.
 */

public class ListContentAdapter extends RecyclerView.Adapter<ListContentAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<dataModal> listdata = null;
    private String titleSelected;
    private int lastPosition = -1;

    public ListContentAdapter(Context mContext, ArrayList<dataModal> listdata,String titleselected) {
        this.mContext = mContext;
        this.listdata = listdata;
        this.titleSelected = titleselected;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listcard,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final dataModal modal = listdata.get(holder.getAdapterPosition());

        holder.txtTitle.setText(modal.get_title());
        holder.txtWepage.setText(modal.get_webpage());
        holder.txtUserId.setText(modal.get_userid());
        holder.txtPassword.setText(modal.get_password());

        //For animation item on scroll
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }
    @Override
    public int getItemCount() {
        return listdata.size();
    }


    //Holder Class
    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitle,txtWepage,txtUserId,txtPassword;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txttitle);
            txtWepage = (TextView) itemView.findViewById(R.id.txtwebpage);
            txtUserId = (TextView)itemView.findViewById(R.id.txtuserid);
            txtPassword = (TextView) itemView.findViewById(R.id.txtpassword);
        }
    }
}
