package com.benchmarksolution.stroreoffline.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.benchmarksolution.stroreoffline.holder.Constants;
import com.benchmarksolution.stroreoffline.holder.dataModal;

import java.util.ArrayList;

/**
 * Created by Admin on 006 06-10-2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static DatabaseHandler mInstance=null;
    private static final String TAG = DatabaseHandler.class.getSimpleName();
    private ArrayList<String> fileName;

    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    public static synchronized DatabaseHandler getInstance(Context context){
        if(mInstance==null){
            mInstance = new DatabaseHandler(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.DB_table);
        String CREATE_STRORE_TABLE = "CREATE TABLE " + Constants.DB_table + "("
                + Constants.DB_id + " INTEGER PRIMARY KEY, "
                + Constants.DB_category + " TEXT NOT NULL, "
                + Constants.DB_title + " TEXT NOT NULL, "
                + Constants.DB_webpage + " TEXT, "
                + Constants.DB_userid + " TEXT, "
                + Constants.DB_password + " TEXT "
                + ")";
        db.execSQL(CREATE_STRORE_TABLE);

    }

    // Adding new record
    public void addToSQL(dataModal datamodal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(Constants.DB_id,datamodal.get_id());
        values.put(Constants.DB_category, datamodal.get_category());
        values.put(Constants.DB_title, datamodal.get_title());
        values.put(Constants.DB_webpage,datamodal.get_webpage());
        values.put(Constants.DB_userid,datamodal.get_userid());
        values.put(Constants.DB_password,datamodal.get_password());
        // Inserting Row
        db.replace(Constants.DB_table, null, values);
        db.close(); // Closing database connection
        Log.d(TAG,"Record added");
    }

    public ArrayList<dataModal> getAllEntrys() {
        ArrayList<dataModal> list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Constants.DB_table;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        try {
            if (cursor!=null && cursor.getCount()>0) {
                cursor.moveToFirst();
                do {
                    dataModal daModal = new dataModal();
                    daModal.set_category(cursor.getString(1));
                    daModal.set_title(cursor.getString(2));
                    daModal.set_webpage(cursor.getString(3));
                    daModal.set_userid(cursor.getString(4));
                    daModal.set_password(cursor.getString(5));

                    // Adding to list
                    list.add(daModal);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // return contact list
        return list;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Constants.DB_table);
        // Create tables again
        onCreate(db);
    }

    // Deleting record if required
    public void deleteField(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.DB_table, Constants.DB_id + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
}
